FROM ubuntu:18.04

ARG SRC_PATH=/usr/local/evshop_src
ARG DEST_PATH=/usr/local/evshop
ARG APP_NAME=evshop.jar

COPY . $SRC_PATH

COPY config/ $DEST_PATH/config

WORKDIR $SRC_PATH

RUN apt-get update && apt-get -y install openjdk-11-jdk
RUN ./gradlew clean build && ./gradlew test
RUN mv ./build/libs/evshop* $DEST_PATH/$APP_NAME
RUN rm -rf $SRC_PATH

WORKDIR $DEST_PATH

CMD ["java", "-jar", "-Dspring.profiles.active=local", "evshop.jar"]

EXPOSE 8081 8081

