apt-get update
apt-get -y install openjdk-11-jdk
./gradlew clean build
./gradlew test
mv ./build/libs/evshop* /usr/local/evshop.jar
java -jar -Dspring.profiles.active=local /usr/local/evshop.jar
