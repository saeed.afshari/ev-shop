# ev-shop
Electronic Vehicle shop application.
You can get access to the project repository [here](https://gitlab.com/saeed.afshari/ev-shop).   

## BUILD, TEST, DEPLOY Automation
In order to build, test, deploy the application, execute the run.sh script.
```
sudo bash ./run.sh
```

Also a Dockerfile created in the root of the project to build, test, and launch application without any headache.
If you have already installed docker run the following command:
```
 docker build -t ev-shop .
``` 

## Requirements

The application has been developed with Java 11, so to be able to launch the application you need to install the following dependencies:
- [OpenJdk 11]
You can download the OpenJdk using the following commands:
```
apt-get update
apt-get -y install openjdk-11-jdk
```

- Docker (If you want to install MySQL container and test under prod profile)
- docker-compose (If you want to install MySQL container and test under prod profile)

## How to build the application and run the tests
In order to build the application run the following command:
```
./gradlew clean build
```
After successful build, the executable jar file will be created under ./build/lib directory.

In order to run unit tests use the following command:
```
./gradlew test
```

## How to start the application

You can start application under two different environment configurations:
- local
- prod  

### local
To run application quickly without any need to ramp up any database server use the following commands:
1. If the jar file available under ./build/lib:
```
java -jar -Dspring.profiles.active=local build/libs/evshop-0.0.1.jar
```
2. use gradle command
```
./gradlew bootRun -Dspring.profiles.active=local
```

The above command will run the application through the following steps:
- H2 As an in-memory database will be loaded
- Liquibase will create necessary tables and insert sample records into the database (init scripts are available under /resources/db/changelog)
- The application will be started on port 8081

### prod
To run application and test application with a production ready database server such as MySQL Server, first the database server should be become up and running.
To do so, the docker-compose file under ./docker/mysql-db/docker-compose.yml path created.
After installing docker and docker-compose by executing the following command the database container will be created.
```
docker-compose -f docker/mysql-db/docker-compose.yml up -d
```
The above command will ramp up MySQL server container, listening on 4321 port number.
It also creates a database named "evshop" which will be used as the project database.

When the database is ready, in order to run the application the following commands can be used:
1. If the jar file available under ./build/lib:
```
java -jar -Dspring.profiles.active=prod build/libs/evshop-0.0.1.jar
```
2. use gradle command
```
./gradlew bootRun -Dspring.profiles.active=prod
```

The above command will run the application through the following steps:
- Liquibase will create necessary tables and insert sample records into the evshop database inside MySQL container (init scripts are available under /resources/db/changelog)
- The application will be started on port 8081  

## API Health check
The application is using Actuator library to provide health check mechanism.
After launching the application, you can use the following url to check the API healthiness: 

```
http://localhost:8081/actuator/health
```

## API Documentation
To document the developed API end-points swagger2 is being used.
You can get access the API documentation via the following end-point:
```
http://localhost:8081/v2/api-docs
```

### API visualisation and interaction 
To visualise the API documentation you can copy the json object generated out of visiting the above link and paste it in
[Swagger Editor](https://editor.swagger.io/) to be able to interact with available APIs.

Or you can also see the API documentation using the following link:
```
http://localhost:8081/swagger-ui.html
```

## CI/CD pipeline
CI/CD pipeline for the project implemented using GitlabCI.
The following steps in the pipeline implemented using .gitlab-ci.yml:
- build : to build the project and cache dependencies
- Checkstyle : Check style using custom config available in ./config/checkstyle path
- JUnit Test : Run all unit tests 

To see the pipeline status, visit the following link:
[https://gitlab.com/saeed.afshari/ev-shop/pipelines](https://gitlab.com/saeed.afshari/ev-shop/pipelines)

## Developed APIs (http://0.0.0.0:8081/v1/...)
The following API end-points implemented:

***Car end-points***
- GET v1/car : reads cars list from the database  
- GET v1/car/{id} : reads the car with the specified id from the database.  
- POST v1/car : Create a new car  
- PUT v1/car/{id} : Update the car information with the specified id  
- POST v1/car/{carId}/order : Configure the specified car with {carId}; (***This API is equivalent to /configure mentioned in the challenge***)

***CarOrder or configure end-points***
- GET v1/car-order : reads cars list from the database  
- GET v1/car-order/{id} : reads an order with the specified id from the database. (CarOrder is equivalent with configure mentioned in the challenge)

***ExtraOption or customisation end-points***
- GET v1/extra-option : reads all customisations/options from the database  

## Database tables
- car : Vehicle with the basic configurations will be stored in this table.
- extra_option : Vehicles customisation options will be stored in this table. 
- customer : Customer information will be stored here. (First step towards auth) 
- car_order : Cars configuration which are customised by users will be stored in this table .
- car_orders_extra_options: Join table to keep Cars configurations and extra options relation in the database
- extra_option_compatibilities: Customisation options compatibility will be check using the information persisted in this table.
```
1. Battery size:
 a. 40 kwh: included in the base price 
 b. 60 kwh: + 2.500 euros
 c. 80 kwh: + 6.000 euros
2. Wheel:
 a. Wheel model 1: included in base price
 b. Wheel model 2: + 150 euros
 c. Wheel model 3 (only available with 60 and 80 kwh batteries): + 350 euros
3. Tires:
 a. Eco: included in base price
 b. Performance (only available with wheel model 2 and model 3): + 80 euros 
 c. Racing (only available with wheel model 3): + 150 euros
```
