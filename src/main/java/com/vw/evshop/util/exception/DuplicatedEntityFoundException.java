package com.vw.evshop.util.exception;

public class DuplicatedEntityFoundException extends RuntimeException {
    public DuplicatedEntityFoundException(String message) {
        super(message);
    }
}
