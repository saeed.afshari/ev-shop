package com.vw.evshop.car.dto.mapper;

import com.vw.evshop.car.domain.Car;
import com.vw.evshop.car.domain.CarOrder;
import com.vw.evshop.car.domain.ExtraOption;
import com.vw.evshop.car.dto.CarCreateOrUpdateDto;
import com.vw.evshop.car.dto.CarOrderResponseDto;
import com.vw.evshop.car.dto.ExtraOptionDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.springframework.stereotype.Component;

import java.util.List;

@Mapper(
        componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE
)
@Component
public interface CarMapper {
    Car toCar(CarCreateOrUpdateDto carCreateOrUpdateDto);

    CarCreateOrUpdateDto toCreateOrUpdateDto(Car car);

    @Mapping(source = "car.id", target = "carId")
    @Mapping(source = "car.model", target = "carModel")
    @Mapping(source = "created", target = "created", dateFormat = "dd.MM.yyyy")
    CarOrderResponseDto toCarOrderResponseDto(CarOrder carOrder);

    List<CarOrderResponseDto> toCarOrderResponseDtos(List<CarOrder> carOrders);

    List<CarCreateOrUpdateDto> toCreateOrUpdateDtos(List<Car> cars);

    ExtraOptionDto toExtraOptionDto(ExtraOption extraOption);

    List<ExtraOptionDto> toExtraOptionDtos(List<ExtraOption> extraOptions);
}
