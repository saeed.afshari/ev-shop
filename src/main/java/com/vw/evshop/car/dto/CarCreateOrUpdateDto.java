package com.vw.evshop.car.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class CarCreateOrUpdateDto {
    private long id;
    @NotNull
    private String model;

    @NotNull
    @Positive
    private BigDecimal basePrice;

    @NotNull
    private String basePriceCurrency;
}
