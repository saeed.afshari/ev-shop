package com.vw.evshop.car.dto;

import com.vw.evshop.car.domain.ExtraOptionType;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class ExtraOptionDto {
    private long id;

    private String name;

    private String description;

    private ExtraOptionType extraOptionType;

    private BigDecimal cost;

    private String costCurrency;
}
