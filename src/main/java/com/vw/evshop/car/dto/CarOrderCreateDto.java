package com.vw.evshop.car.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@NoArgsConstructor
public class CarOrderCreateDto {
    @NotNull
    @NotEmpty
    private List<Long> extraOptions;

    @NotNull
    @NotEmpty
    private String customerName;

    private String description;
}
