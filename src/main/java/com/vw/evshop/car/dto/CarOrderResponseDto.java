package com.vw.evshop.car.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

@Data
@NoArgsConstructor
public class CarOrderResponseDto {
    private long id;
    private long carId;
    private String customerName;
    private String carModel;
    private String orderNumber;
    private String description;
    private BigDecimal totalPrice;
    private String totalPriceCurrency;
    private String created;
    private List<ExtraOptionDto> extraOptions;
}
