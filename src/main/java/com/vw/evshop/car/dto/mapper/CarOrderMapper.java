package com.vw.evshop.car.dto.mapper;

import com.vw.evshop.car.domain.Car;
import com.vw.evshop.car.dto.CarCreateOrUpdateDto;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.springframework.stereotype.Component;

@Mapper(
        componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE
)
@Component
public interface CarOrderMapper {
    Car toCar(CarCreateOrUpdateDto carCreateOrUpdateDto);

    CarCreateOrUpdateDto toCreateOrUpdateDto(Car car);
}
