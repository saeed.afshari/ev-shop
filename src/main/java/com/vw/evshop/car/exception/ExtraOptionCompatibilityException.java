package com.vw.evshop.car.exception;

public class ExtraOptionCompatibilityException extends RuntimeException {
    public ExtraOptionCompatibilityException(String message) {
        super(message);
    }
}
