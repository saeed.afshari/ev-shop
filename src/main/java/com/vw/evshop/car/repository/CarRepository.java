package com.vw.evshop.car.repository;

import com.vw.evshop.car.domain.Car;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Implements repository pattern for Car
 */
@Repository
public interface CarRepository extends JpaRepository<Car, Long> {
    Car findByModel(String model);
}
