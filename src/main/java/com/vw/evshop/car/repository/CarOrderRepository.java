package com.vw.evshop.car.repository;

import com.vw.evshop.car.domain.CarOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Implements repository pattern for CarOrder
 */
@Repository
public interface CarOrderRepository extends JpaRepository<CarOrder, Long> {
    CarOrder findByOrderNumber(String orderNumber);
}
