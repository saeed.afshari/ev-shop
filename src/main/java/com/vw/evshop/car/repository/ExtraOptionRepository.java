package com.vw.evshop.car.repository;

import com.vw.evshop.car.domain.ExtraOption;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Implements repository pattern for ExtraOption
 */
@Repository
public interface ExtraOptionRepository extends JpaRepository<ExtraOption, Long> {

    List<ExtraOption> findByIdIn(List<Long> ids);
}
