package com.vw.evshop.car.domain;

public enum ExtraOptionType {
    BATTERY_SIZE,
    WHEEL,
    TIRE
}
