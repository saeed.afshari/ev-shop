package com.vw.evshop.car.domain;

import lombok.Data;
import org.joda.money.CurrencyUnit;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * The entity for storing cars in database
 */
@Entity
@Data
@Table(name = "car", indexes = {@Index(name = "idx_car_model", columnList = "model", unique = true)})
public class Car {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false)
    private String model;

    @Column(nullable = false)
    private BigDecimal basePrice = BigDecimal.ZERO;

    @Column(nullable = false)
    private String basePriceCurrency = CurrencyUnit.EUR.getCode();

    @OneToMany(mappedBy = "car", cascade = CascadeType.ALL)
    private List<CarOrder> carOrders;

    @Column(nullable = false)
    private LocalDateTime created = LocalDateTime.now();

    @Column(nullable = false)
    private LocalDateTime updated = LocalDateTime.now();

    @Column
    private LocalDateTime deleted;

}
