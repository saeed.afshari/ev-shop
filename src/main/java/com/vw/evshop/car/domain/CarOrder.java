package com.vw.evshop.car.domain;

import com.vw.evshop.customer.domain.Customer;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.joda.money.CurrencyUnit;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity(name = "car_order")
@Data
@NoArgsConstructor
public class CarOrder {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false)
    private String orderNumber = UUID.randomUUID().toString();

    @Column(nullable = false)
    private String customerName;

    @Column
    private String description;

    @Column(nullable = false)
    private BigDecimal totalPrice = BigDecimal.ZERO;

    @Column(nullable = false)
    private String totalPriceCurrency = CurrencyUnit.EUR.getCode();

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "car_id")
    private Car car;

    @ManyToOne
    @JoinColumn(name = "customer_id")
    private Customer customer;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "car_orders_extra_options",
            joinColumns = @JoinColumn(name = "car_order_id"),
            inverseJoinColumns = @JoinColumn(name = "extra_option_id"))
    private List<ExtraOption> extraOptions = new ArrayList<>();

    @Column(nullable = false)
    private LocalDateTime created = LocalDateTime.now();

    @Column(nullable = false)
    private LocalDateTime updated = LocalDateTime.now();

    @Column
    private LocalDateTime deleted;

    /**
     * Calculates total price using the following formula:
     * Car basePrice + battery cost + wheel cost + tire cost
     */
    public void calculateTotalPrice() {
        BigDecimal basePrice = car.getBasePrice();
        for (var extraOption : getExtraOptions()) {
            basePrice = basePrice.add(extraOption.getCost());
        }
        totalPrice = basePrice;
    }
}
