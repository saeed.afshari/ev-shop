package com.vw.evshop.car.domain;

import lombok.Data;
import org.joda.money.CurrencyUnit;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Implements extra options table for car orders
 */
@Data
@Entity(name = "extra_option")
public class ExtraOption {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false)
    private String name;

    @Column
    private String description;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private ExtraOptionType extraOptionType;

    @Column(nullable = false)
    private BigDecimal cost = BigDecimal.ZERO;

    @Column(nullable = false)
    private String costCurrency = CurrencyUnit.EUR.getCode();

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "extra_option_compatibilities",
            joinColumns = @JoinColumn(name = "extra_option_id"),
            inverseJoinColumns = @JoinColumn(name = "compatible_option_id"))
    private List<ExtraOption> compatibleOptions = new ArrayList<>();

    @Column(nullable = false)
    private LocalDateTime created = LocalDateTime.now();

    @Column(nullable = false)
    private LocalDateTime updated = LocalDateTime.now();

    @Column
    private LocalDateTime deleted;
}
