package com.vw.evshop.car.service;

import com.vw.evshop.car.domain.CarOrder;

import java.util.List;

public interface CarOrderService {
    CarOrder save(CarOrder carOrder);

    CarOrder findById(long id);

    CarOrder findByOrderNumber(String orderNumber);

    List<CarOrder> findAll();
}
