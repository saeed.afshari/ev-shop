package com.vw.evshop.car.service;

import com.vw.evshop.car.domain.ExtraOption;
import com.vw.evshop.car.repository.ExtraOptionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Implements service for manipulating extra options
 */
@Service
public class ExtraOptionServiceImpl implements ExtraOptionService {

    private final ExtraOptionRepository extraOptionRepository;

    @Autowired
    public ExtraOptionServiceImpl(ExtraOptionRepository extraOptionRepository) {
        this.extraOptionRepository = extraOptionRepository;
    }

    @Override
    public List<ExtraOption> findAll() {
        return extraOptionRepository.findAll();
    }

    @Override
    public List<ExtraOption> findByIds(List<Long> ids) {
        return extraOptionRepository.findByIdIn(ids);
    }
}
