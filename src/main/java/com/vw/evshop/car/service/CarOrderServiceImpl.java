package com.vw.evshop.car.service;

import com.vw.evshop.car.domain.CarOrder;
import com.vw.evshop.car.domain.ExtraOption;
import com.vw.evshop.car.domain.ExtraOptionType;
import com.vw.evshop.car.exception.ExtraOptionCompatibilityException;
import com.vw.evshop.car.repository.CarOrderRepository;
import com.vw.evshop.util.exception.EntityNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CarOrderServiceImpl implements CarOrderService {
    private static final String ORDER_NOT_FOUND = "Order not found";

    private final CarOrderRepository carOrderRepository;

    public CarOrderServiceImpl(CarOrderRepository carOrderRepository) {

        this.carOrderRepository = carOrderRepository;
    }

    @Override
    public CarOrder save(CarOrder carOrder) {
        validateExtraOptions(carOrder);
        carOrder.setId(0);
        carOrder.calculateTotalPrice();
        return carOrderRepository.save(carOrder);
    }

    /**
     * Checks option compatibilities as follows:
     * 1. Battery size:
     * a. 40 kwh: included in the base price b. 60 kwh: + 2.500 euros
     * c. 80 kwh: + 6.000 euros
     * 2. Wheel:
     * a. Wheel model 1: included in base price
     * b. Wheel model 2: + 150 euros
     * c. Wheel model 3 (only available with 60 and 80 kwh batteries): + 350 euros
     * 3. Tires:
     * a. Eco: included in base price
     * b. Performance (only available with wheel model 2 and model 3): + 80 euros c. Racing (only available with wheel model 3): + 150 euros
     */
    void validateExtraOptions(CarOrder carOrder) {
        List<ExtraOption> extraOptions = carOrder.getExtraOptions();
        if (extraOptions.size() == 0) {
            throw new EntityNotFoundException("Battery, Wheel, Tire options not found in the order");
        }
        ExtraOption battery = tryGetBattery(extraOptions);
        ExtraOption wheel = tryGetWheel(extraOptions, battery);
        tryGetTire(extraOptions, wheel);
    }

    private ExtraOption tryGetBattery(List<ExtraOption> extraOptions) {
        Optional<ExtraOption> batteryOpt = extraOptions.stream().
                filter(item -> item.getExtraOptionType() == ExtraOptionType.BATTERY_SIZE)
                .findFirst();
        if (batteryOpt.isEmpty()) {
            throw new EntityNotFoundException("Battery option not found in the order");
        }
        return batteryOpt.get();
    }

    private ExtraOption tryGetWheel(List<ExtraOption> extraOptions, ExtraOption battery) {
        Optional<ExtraOption> wheelOpt = extraOptions.stream().
                filter(item -> item.getExtraOptionType() == ExtraOptionType.WHEEL)
                .findFirst();
        if (wheelOpt.isEmpty()) {
            throw new EntityNotFoundException("Wheel option not found in the order");
        }
        ExtraOption wheel = wheelOpt.get();
        if (wheel.getCompatibleOptions().stream().noneMatch(item -> item.getId() == battery.getId())) {
            throw new ExtraOptionCompatibilityException(wheel.getDescription());
        }
        return wheel;
    }

    private void tryGetTire(List<ExtraOption> extraOptions, ExtraOption wheel) {
        Optional<ExtraOption> tireOpt = extraOptions.stream().
                filter(item -> item.getExtraOptionType() == ExtraOptionType.TIRE)
                .findFirst();
        if (tireOpt.isEmpty()) {
            throw new EntityNotFoundException("Tire option not found in the order");
        }
        ExtraOption tire = tireOpt.get();
        if (tire.getCompatibleOptions().stream().noneMatch(item -> item.getId() == wheel.getId())) {
            throw new ExtraOptionCompatibilityException(tire.getDescription());
        }
    }

    @Override
    public CarOrder findById(long id) {
        Optional<CarOrder> carOrder = carOrderRepository.findById(id);
        if (carOrder.isEmpty()) {
            throw new EntityNotFoundException(ORDER_NOT_FOUND);
        }
        return carOrder.get();
    }

    @Override
    public CarOrder findByOrderNumber(String orderNumber) {
        var carOrder = carOrderRepository.findByOrderNumber(orderNumber);
        if (carOrder == null) {
            throw new EntityNotFoundException(ORDER_NOT_FOUND);
        }
        return carOrder;
    }

    @Override
    public List<CarOrder> findAll() {
        return carOrderRepository.findAll();
    }
}
