package com.vw.evshop.car.service;

import com.vw.evshop.car.domain.Car;
import com.vw.evshop.car.repository.CarRepository;
import com.vw.evshop.util.exception.DuplicatedEntityFoundException;
import com.vw.evshop.util.exception.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Implements service for manipulating cars
 */
@Service
public class CarServiceImpl implements CarService {

    private final CarRepository carRepository;

    @Autowired
    public CarServiceImpl(CarRepository carRepository) {
        this.carRepository = carRepository;
    }

    public Car save(Car car) {
        car.setId(0);
        if (carRepository.findByModel(car.getModel()) != null) {
            throw new DuplicatedEntityFoundException("A car with the same model is already registered.");
        }
        return carRepository.save(car);
    }

    @Override
    public Car update(Car car) {
        if (carRepository.findById(car.getId()).isEmpty()) {
            throw new EntityNotFoundException("Car not found");
        }
        return carRepository.save(car);
    }

    @Override
    public Car findById(long id) {
        Optional<Car> car = carRepository.findById(id);
        if (car.isEmpty()) {
            throw new EntityNotFoundException("Car not found");
        }
        return car.get();
    }

    @Override
    public List<Car> findAll() {
        return carRepository.findAll();
    }
}
