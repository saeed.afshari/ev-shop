package com.vw.evshop.car.service;

import com.vw.evshop.car.domain.Car;

import java.util.List;

public interface CarService {
    Car save(Car car);

    Car update(Car car);

    Car findById(long id);

    List<Car> findAll();
}
