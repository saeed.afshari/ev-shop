package com.vw.evshop.car.service;

import com.vw.evshop.car.domain.ExtraOption;

import java.util.List;

public interface ExtraOptionService {
    List<ExtraOption> findAll();

    List<ExtraOption> findByIds(List<Long> ids);
}
