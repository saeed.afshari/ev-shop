package com.vw.evshop.customer.domain;

import com.vw.evshop.car.domain.CarOrder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.List;

/**
 * The entity for storing customers in database; This entity also will be used for authentication
 */
@Entity
@Data
@NoArgsConstructor
@Table(name = "customer",
        indexes = {@Index(name = "idx_customer_email", columnList = "email", unique = true)})
public class Customer {
    private static final String DEFAULT_PASSWORD = "password";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String email;

    /**
     * For the first iteration the password for all customers will "password".
     * In the next steps password should be asked from customers and encrypted
     */
    @Column(nullable = false)
    private String password = DEFAULT_PASSWORD;

    @OneToMany(mappedBy = "customer", cascade = CascadeType.ALL)
    private List<CarOrder> carOrders;

    @Column(nullable = false)
    private LocalDateTime created = LocalDateTime.now();

    @Column(nullable = false)
    private LocalDateTime updated = LocalDateTime.now();

    @Column
    private LocalDateTime deleted;
}
