package com.vw.evshop.controller;

import com.vw.evshop.car.dto.ExtraOptionDto;
import com.vw.evshop.car.dto.mapper.CarMapper;
import com.vw.evshop.car.service.ExtraOptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Implements CRUD end-points for car entity to let users manipulate cars via REST
 */
@RestController
@RequestMapping(value = "v1/extra-option", produces = MediaType.APPLICATION_JSON_VALUE)
public class ExtraOptionController {
    private final ExtraOptionService extraOptionService;
    private final CarMapper carMapper;

    @Autowired
    public ExtraOptionController(ExtraOptionService extraOptionService,
                                 CarMapper carMapper) {
        this.extraOptionService = extraOptionService;
        this.carMapper = carMapper;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<ExtraOptionDto> getAll() {
        return carMapper.toExtraOptionDtos(extraOptionService.findAll());
    }
}
