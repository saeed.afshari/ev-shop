package com.vw.evshop.controller;

import com.vw.evshop.util.exception.DuplicatedEntityFoundException;
import com.vw.evshop.util.exception.EntityNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Implements a global error handling controller in order to return understandable response to the clients
 */
@ControllerAdvice
@Slf4j
public class ExceptionHandlingController {

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(EntityNotFoundException.class)
    public String handleNotFound(EntityNotFoundException exception) {
        return exception.getMessage();
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(DataIntegrityViolationException.class)
    public String handleNotFound(DataIntegrityViolationException exception) {
        log.error(exception.getMessage(), exception);
        return "Invalid input data. Please update";
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(DuplicatedEntityFoundException.class)
    public String handleNotFound(DuplicatedEntityFoundException exception) {
        return exception.getMessage();
    }

}
