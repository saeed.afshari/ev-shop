package com.vw.evshop.controller;

import com.vw.evshop.car.dto.CarOrderResponseDto;
import com.vw.evshop.car.dto.mapper.CarMapper;
import com.vw.evshop.car.service.CarOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Implements CRUD end-points for car entity to let users manipulate cars via REST
 */
@RestController
@RequestMapping(value = "v1/car-order", produces = MediaType.APPLICATION_JSON_VALUE)
public class CarOrderController {
    private final CarOrderService carOrderService;
    private final CarMapper carMapper;

    @Autowired
    public CarOrderController(CarOrderService carOrderService,
                              CarMapper carMapper) {
        this.carOrderService = carOrderService;
        this.carMapper = carMapper;
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public CarOrderResponseDto get(@PathVariable long id) {
        return carMapper.toCarOrderResponseDto(carOrderService.findById(id));
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<CarOrderResponseDto> getAll() {
        return carMapper.toCarOrderResponseDtos(carOrderService.findAll());
    }
}
