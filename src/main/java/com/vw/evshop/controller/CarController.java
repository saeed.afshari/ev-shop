package com.vw.evshop.controller;

import com.vw.evshop.car.domain.Car;
import com.vw.evshop.car.domain.CarOrder;
import com.vw.evshop.car.dto.CarCreateOrUpdateDto;
import com.vw.evshop.car.dto.CarOrderCreateDto;
import com.vw.evshop.car.dto.CarOrderResponseDto;
import com.vw.evshop.car.dto.mapper.CarMapper;
import com.vw.evshop.car.service.CarOrderService;
import com.vw.evshop.car.service.CarService;
import com.vw.evshop.car.service.ExtraOptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

/**
 * Implements CRUD end-points for car entity to let users manipulate cars via REST
 */
@RestController
@RequestMapping(value = "v1/car", produces = MediaType.APPLICATION_JSON_VALUE)
public class CarController {
    private final CarService carService;
    private final CarOrderService carOrderService;
    private final ExtraOptionService extraOptionService;
    private final CarMapper carMapper;

    @Autowired
    public CarController(CarService carService,
                         CarOrderService carOrderService,
                         ExtraOptionService extraOptionService,
                         CarMapper carMapper) {
        this.carService = carService;
        this.carOrderService = carOrderService;
        this.extraOptionService = extraOptionService;
        this.carMapper = carMapper;
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public CarCreateOrUpdateDto get(@PathVariable long id) {
        return carMapper.toCreateOrUpdateDto(carService.findById(id));
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<CarCreateOrUpdateDto> getAll() {
        return carMapper.toCreateOrUpdateDtos(carService.findAll());
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CarCreateOrUpdateDto create(@RequestBody @Valid CarCreateOrUpdateDto carCreateOrUpdateDto) {
        Car car = carMapper.toCar(carCreateOrUpdateDto);
        return carMapper.toCreateOrUpdateDto(carService.save(car));
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public CarCreateOrUpdateDto update(@PathVariable long id, @RequestBody @Valid CarCreateOrUpdateDto carCreateOrUpdateDto) {
        Car car = carMapper.toCar(carCreateOrUpdateDto);
        car.setId(id);
        return carMapper.toCreateOrUpdateDto(carService.update(car));
    }

    @PostMapping("/{carId}/order")
    @ResponseStatus(HttpStatus.CREATED)
    public CarOrderResponseDto createOrder(@PathVariable long carId,
                                           @RequestBody @Valid CarOrderCreateDto carOrderCreateDto) {
        if (carId <= 0) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Specified car not found");
        }

        var carOrder = new CarOrder();
        carOrder.setCustomerName(carOrderCreateDto.getCustomerName());
        carOrder.setDescription(carOrderCreateDto.getDescription());
        carOrder.setCar(carService.findById(carId));
        carOrder.setExtraOptions(extraOptionService.findByIds(carOrderCreateDto.getExtraOptions()));
        return carMapper.toCarOrderResponseDto(carOrderService.save(carOrder));
    }
}
