-- liquibase formatted sql

-- changeset saeedafshari:create-car-table

CREATE TABLE car
(
    id                  BIGINT AUTO_INCREMENT PRIMARY KEY,
    model               VARCHAR(255)              NOT NULL UNIQUE,
    base_price          DECIMAL(15, 2)            NOT NULL,
    base_price_currency VARCHAR(10) DEFAULT 'EUR' NOT NULL,
    created             datetime                  NOT NULL,
    updated             datetime                  NOT NULL,
    deleted             datetime                  NULL
);

insert into car (id, model, base_price, base_price_currency, created, updated, deleted)
values (1, 'Standard', 12000, 'EUR', NOW(), NOW(), NULL);

insert into car (id, model, base_price, base_price_currency, created, updated, deleted)
values (2, 'Sport', 13000, 'EUR', NOW(), NOW(), NULL);

insert into car (id, model, base_price, base_price_currency, created, updated, deleted)
values (3, 'Hybrid', 14000, 'EUR', NOW(), NOW(), NULL);
