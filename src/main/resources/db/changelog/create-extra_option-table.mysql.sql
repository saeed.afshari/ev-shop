-- liquibase formatted sql

-- changeset saeedafshari:create-extra_option-table

CREATE TABLE extra_option
(
    id                BIGINT AUTO_INCREMENT PRIMARY KEY,
    name              VARCHAR(500)              NOT NULL,
    description       VARCHAR(500)              NULL,
    extra_option_type VARCHAR(128)              NOT NULL,
    cost              DECIMAL(15, 2)            NOT NULL,
    cost_currency     VARCHAR(10) DEFAULT 'EUR' NOT NULL,
    created           datetime                  NOT NULL,
    updated           datetime                  NOT NULL,
    deleted           datetime                  NULL
);

CREATE TABLE customer
(
    id       BIGINT AUTO_INCREMENT PRIMARY KEY,
    name     VARCHAR(255) NOT NULL,
    email    VARCHAR(255) NOT NULL UNIQUE,
    password VARCHAR(128) NOT NULL,
    created  datetime     NOT NULL,
    updated  datetime     NOT NULL,
    deleted  datetime     NULL
);

CREATE TABLE car_order
(
    id                   BIGINT AUTO_INCREMENT PRIMARY KEY,
    order_number         VARCHAR(36)               NOT NULL,
    description          VARCHAR(500)              NULL,
    customer_name        VARCHAR(255)              NOT NULL,
    total_price          DECIMAL(15, 2)            NOT NULL,
    total_price_currency VARCHAR(10) DEFAULT 'EUR' NOT NULL,
    car_id               BIGINT,
    customer_id          BIGINT,
    created              datetime                  NOT NULL,
    updated              datetime                  NOT NULL,
    deleted              datetime                  NULL
);

ALTER TABLE car_order
    ADD FOREIGN KEY (car_id) REFERENCES car (id);
ALTER TABLE car_order
    ADD FOREIGN KEY (customer_id) REFERENCES customer (id);

CREATE TABLE car_orders_extra_options
(
    car_order_id    BIGINT NOT NULL,
    extra_option_id BIGINT NOT NULL,
    CONSTRAINT FK_Orders_Extra_Options_Order FOREIGN KEY (car_order_id) REFERENCES car_order (id),
    CONSTRAINT FK_Orders_Extra_Options_Extra_Option FOREIGN KEY (extra_option_id) REFERENCES extra_option (id)
);

-- Default customer in order to test auth
insert into customer (name, email, password, created, updated)
values ('saeed', 'saeed.afshari8@gmail.com', 'password', NOW(), NOW());

insert into extra_option (id, name, extra_option_type, description, cost, cost_currency, created, updated)
values (1, '40 kwh', 'BATTERY_SIZE', 'included in the base price', 0, 'EUR', NOW(), NOW());

insert into extra_option (id, name, extra_option_type, description, cost, cost_currency, created, updated)
values (2, '60 kwh', 'BATTERY_SIZE', '', 2500, 'EUR', NOW(), NOW());

insert into extra_option (id, name, extra_option_type, description, cost, cost_currency, created, updated)
values (3, '80 kwh', 'BATTERY_SIZE', '', 6000, 'EUR', NOW(), NOW());

insert into extra_option (id, name, extra_option_type, description, cost, cost_currency, created, updated)
values (4, 'Wheel model 1', 'WHEEL', 'included in the base price', 0, 'EUR', NOW(), NOW());

insert into extra_option (id, name, extra_option_type, description, cost, cost_currency, created, updated)
values (5, 'Wheel model 2', 'WHEEL', '', 150, 'EUR', NOW(), NOW());

insert into extra_option (id, name, extra_option_type, description, cost, cost_currency, created, updated)
values (6, 'Wheel model 3', 'WHEEL', 'only available with 60 and 80 kwh batteries', 350, 'EUR', NOW(), NOW());

insert into extra_option (id, name, extra_option_type, description, cost, cost_currency, created, updated)
values (7, 'Eco', 'TIRE', 'included in the base price', 0, 'EUR', NOW(), NOW());

insert into extra_option (id, name, extra_option_type, description, cost, cost_currency, created, updated)
values (8, 'Performance', 'TIRE', 'only available with wheel model 2 and model 3', 80, 'EUR', NOW(), NOW());

insert into extra_option (id, name, extra_option_type, description, cost, cost_currency, created, updated)
values (9, 'Racing', 'TIRE', 'only available with model 3', 150, 'EUR', NOW(), NOW());


CREATE TABLE extra_option_compatibilities
(
    extra_option_id      BIGINT NOT NULL,
    compatible_option_id BIGINT NOT NULL,
    CONSTRAINT FK_Extra_Options_Compatibilities_Extra_Option_Id FOREIGN KEY (extra_option_id) REFERENCES extra_option (id),
    CONSTRAINT FK_Extra_Options_Compatibilities_Compatible_Option_Id FOREIGN KEY (compatible_option_id) REFERENCES extra_option (id)
);

insert into extra_option_compatibilities (extra_option_id, compatible_option_id)
values (4, 1);
insert into extra_option_compatibilities (extra_option_id, compatible_option_id)
values (4, 2);
insert into extra_option_compatibilities (extra_option_id, compatible_option_id)
values (4, 3);
insert into extra_option_compatibilities (extra_option_id, compatible_option_id)
values (5, 1);
insert into extra_option_compatibilities (extra_option_id, compatible_option_id)
values (5, 2);
insert into extra_option_compatibilities (extra_option_id, compatible_option_id)
values (5, 3);
insert into extra_option_compatibilities (extra_option_id, compatible_option_id)
values (6, 2);
insert into extra_option_compatibilities (extra_option_id, compatible_option_id)
values (6, 3);
insert into extra_option_compatibilities (extra_option_id, compatible_option_id)
values (7, 4);
insert into extra_option_compatibilities (extra_option_id, compatible_option_id)
values (7, 5);
insert into extra_option_compatibilities (extra_option_id, compatible_option_id)
values (7, 6);
insert into extra_option_compatibilities (extra_option_id, compatible_option_id)
values (8, 5);
insert into extra_option_compatibilities (extra_option_id, compatible_option_id)
values (8, 6);
insert into extra_option_compatibilities (extra_option_id, compatible_option_id)
values (9, 6);
