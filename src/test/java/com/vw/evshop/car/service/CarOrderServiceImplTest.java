package com.vw.evshop.car.service;

import com.vw.evshop.car.domain.CarOrder;
import com.vw.evshop.car.domain.ExtraOption;
import com.vw.evshop.car.domain.ExtraOptionType;
import com.vw.evshop.car.exception.ExtraOptionCompatibilityException;
import com.vw.evshop.car.repository.CarOrderRepository;
import com.vw.evshop.util.exception.EntityNotFoundException;
import org.joda.money.CurrencyUnit;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;

class CarOrderServiceImplTest {

    private CarOrderServiceImpl carOrderService;

    private final ExtraOption batterySize = new ExtraOption();
    private final ExtraOption wheel = new ExtraOption();
    private final ExtraOption tire = new ExtraOption();

    private final CarOrder carOrder = new CarOrder();

    @Mock
    private CarOrderRepository carOrderRepository;

    @BeforeEach
    void beforeEach() {
        carOrderService = new CarOrderServiceImpl(carOrderRepository);

        batterySize.setId(1);
        batterySize.setExtraOptionType(ExtraOptionType.BATTERY_SIZE);
        batterySize.setCost(BigDecimal.ONE);
        batterySize.setCostCurrency(CurrencyUnit.EUR.getCode());

        wheel.setId(2);
        wheel.setExtraOptionType(ExtraOptionType.WHEEL);
        wheel.setCost(BigDecimal.TEN);
        wheel.setCostCurrency(CurrencyUnit.EUR.getCode());
        wheel.setCompatibleOptions(Arrays.asList(batterySize));

        tire.setId(3);
        tire.setExtraOptionType(ExtraOptionType.TIRE);
        tire.setCost(BigDecimal.TEN);
        tire.setCostCurrency(CurrencyUnit.EUR.getCode());
        tire.setCompatibleOptions(Arrays.asList(wheel));

        carOrder.setExtraOptions(Arrays.asList(batterySize, wheel, tire));
    }

    @Test
    void validExtraOptionsProvidedThenValidationShouldPass() {
        carOrderService.validateExtraOptions(carOrder);
    }

    @Test
    void noOptionsProvidedThenValidationShouldThrowEntityNotFoundException() {
        carOrder.setExtraOptions(new ArrayList<>());
        Assertions.assertThrows(EntityNotFoundException.class, () -> carOrderService.validateExtraOptions(carOrder));
    }

    @Test
    void batterySizeNotProvidedThenValidationShouldThrowEntityNotFoundException() {
        carOrder.setExtraOptions(Arrays.asList(wheel, tire));
        Assertions.assertThrows(EntityNotFoundException.class, () -> carOrderService.validateExtraOptions(carOrder));
    }

    @Test
    void wheelNotProvidedThenValidationShouldThrowEntityNotFoundException() {
        carOrder.setExtraOptions(Arrays.asList(batterySize, tire));
        Assertions.assertThrows(EntityNotFoundException.class, () -> carOrderService.validateExtraOptions(carOrder));
    }

    @Test
    void wheelHasNoCompatibleOptionsThenValidationShouldThrowExtraOptionCompatibilityException() {
        wheel.setCompatibleOptions(new ArrayList<>());
        carOrder.setExtraOptions(Arrays.asList(batterySize, wheel, tire));
        Assertions.assertThrows(ExtraOptionCompatibilityException.class,
                () -> carOrderService.validateExtraOptions(carOrder));
    }

    @Test
    void tireHasNoCompatibleOptionsThenValidationShouldThrowExtraOptionCompatibilityException() {
        tire.setCompatibleOptions(new ArrayList<>());
        carOrder.setExtraOptions(Arrays.asList(batterySize, wheel, tire));
        Assertions.assertThrows(ExtraOptionCompatibilityException.class,
                () -> carOrderService.validateExtraOptions(carOrder));
    }
}
