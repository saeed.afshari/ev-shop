package com.vw.evshop.car.domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Arrays;

class CarOrderTest {
    @Test
    void calculateTotalPrice() {
        Car car = new Car();
        car.setBasePrice(BigDecimal.ONE);

        ExtraOption batterySize = new ExtraOption();
        batterySize.setCost(BigDecimal.ONE);
        ExtraOption wheel = new ExtraOption();
        wheel.setCost(BigDecimal.ONE);
        ExtraOption tire = new ExtraOption();
        tire.setCost(BigDecimal.ONE);

        CarOrder carOrder = new CarOrder();
        carOrder.setCar(car);
        carOrder.setExtraOptions(Arrays.asList(batterySize, wheel, tire));

        carOrder.calculateTotalPrice();

        Assertions.assertEquals(BigDecimal.valueOf(4L), carOrder.getTotalPrice());
    }
}
