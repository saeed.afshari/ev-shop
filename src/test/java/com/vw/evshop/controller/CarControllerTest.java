package com.vw.evshop.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vw.evshop.car.domain.Car;
import com.vw.evshop.car.dto.CarCreateOrUpdateDto;
import com.vw.evshop.car.dto.CarOrderCreateDto;
import com.vw.evshop.car.dto.mapper.CarMapper;
import com.vw.evshop.car.service.CarOrderService;
import com.vw.evshop.car.service.CarService;
import com.vw.evshop.car.service.ExtraOptionService;
import org.joda.money.CurrencyUnit;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.math.BigDecimal;
import java.util.Arrays;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * API tests for CarController
 */
@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = {CarController.class})
public class CarControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CarService carService;

    @MockBean
    private CarOrderService carOrderService;

    @MockBean
    private ExtraOptionService extraOptionService;

    @MockBean
    private CarMapper carMapper;

    @Autowired
    private static ObjectMapper objectMapper;

    private Car car;

    @BeforeAll
    static void beforeAll() {
        objectMapper = new ObjectMapper();
    }

    @BeforeEach
    void beforeEach() {
        car = new Car();
        car.setModel("testModel");
        car.setBasePrice(BigDecimal.TEN);
        car.setBasePriceCurrency(CurrencyUnit.EUR.getCode());
    }

    @Test
    void validInputProvidedViaGetCallThenReturns200() throws Exception {
        mockMvc.perform(get("/v1/car/1")
                .contentType("application/json"))
                .andExpect(status().isOk());
    }

    @Test
    void validInputProvidedViaPostCallThenReturns201() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .post("/v1/car")
                .content(asJsonString(car))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    void modelIsNotProvidedThenReturn400Error() throws Exception {
        car.setModel(null);

        mockMvc.perform(MockMvcRequestBuilders
                .post("/v1/car")
                .content(asJsonString(car))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError());
    }

    @Test
    void basePriceIsNotProvidedThenReturn400Error() throws Exception {
        car.setBasePrice(null);

        mockMvc.perform(MockMvcRequestBuilders
                .post("/v1/car")
                .content(asJsonString(car))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError());
    }

    @Test
    void validInputProvidedViaPutCallThenReturns200() throws Exception {
        Mockito.when(carMapper.toCar(any(CarCreateOrUpdateDto.class))).thenReturn(car);
        mockMvc.perform(MockMvcRequestBuilders
                .put("/v1/car/1")
                .content(asJsonString(car))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void validInputProvidedViaPostCallThenReturns200() throws Exception {
        CarOrderCreateDto carOrderCreateDto = new CarOrderCreateDto();
        carOrderCreateDto.setCustomerName("test");
        carOrderCreateDto.setExtraOptions(Arrays.asList(1L, 2L, 3L));
        mockMvc.perform(MockMvcRequestBuilders
                .post("/v1/car/1/order")
                .content(asJsonString(carOrderCreateDto))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    void invalidCarIdProvidedViaPostCallThenReturns400() throws Exception {
        CarOrderCreateDto carOrderCreateDto = new CarOrderCreateDto();
        carOrderCreateDto.setCustomerName("test");
        carOrderCreateDto.setExtraOptions(Arrays.asList(1L, 2L, 3L));
        mockMvc.perform(MockMvcRequestBuilders
                .post("/v1/car/-1/order")
                .content(asJsonString(carOrderCreateDto))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError());
    }

    @Test
    void customerNameNotProvidedViaPostCallThenReturns400() throws Exception {
        CarOrderCreateDto carOrderCreateDto = new CarOrderCreateDto();
        carOrderCreateDto.setExtraOptions(Arrays.asList(1L, 2L, 3L));
        mockMvc.perform(MockMvcRequestBuilders
                .post("/v1/car/-1/order")
                .content(asJsonString(carOrderCreateDto))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError());
    }

    public String asJsonString(final Object obj) {
        try {
            return objectMapper.writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
